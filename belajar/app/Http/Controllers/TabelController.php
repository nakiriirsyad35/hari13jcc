<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TabelController extends Controller
{
    public function table()
    {
        return view('template/table');
    }

    public function dataTable()
    {
        return view('template/datatable');
    }
}
